import React from "react";
import styles from "./index.module.scss";
import { useNavigate } from "react-router-dom";

export const Home = () => {
    const navigate = useNavigate();
    return <div className={styles.container}>
        <img className={styles.image_1} src={require("src/assets/title.png")} alt="" />
        <img className={styles.image_2} onClick={() => {
            navigate("/login");
        }} src={require("src/assets/start-game.png")} alt="" />
    </div>;
};
