import React from "react";
import styles from "./index.module.scss";
import { useNavigate } from "react-router-dom";
import { LoginForm, ProFormText } from "@ant-design/pro-components";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { message } from "antd";

export const Login = () => {
    const navigate = useNavigate();
    return <div className={styles.container}>
        <LoginForm title="游戏登录" subTitle="一款趣味游戏" onFinish={(formData) => {
            return new Promise(async (resolve) => {
                if (formData.username === "admin" && formData.password === "admin") {
                    navigate("/game");
                } else {
                    message.error({
                        key: 0x0011,
                        content: "用户名或密码错误"
                    });
                }
                resolve(true);
            });
        }}>
            <ProFormText
                name="username"
                fieldProps={{
                    size: "large",
                    prefix: <UserOutlined className={"prefixIcon"} />
                }}
                placeholder={"用户名: admin"}
                rules={[
                    {
                        required: true,
                        message: "请输入用户名!"
                    }
                ]}
            />
            <ProFormText.Password
                name="password"
                fieldProps={{
                    size: "large",
                    prefix: <LockOutlined className={"prefixIcon"} />
                }}
                placeholder={"密码: admin"}
                rules={[
                    {
                        required: true,
                        message: "请输入密码！"
                    }
                ]}
            />
        </LoginForm>
    </div>;
};
