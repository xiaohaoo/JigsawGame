import { createHashRouter, Navigate, RouteObject } from "react-router-dom";
import { Home } from "src/pages/home";
import { Game } from "src/pages/game";
import { Login } from "src/pages/login";

const routes: RouteObject[] = [
    {
        path: "home",
        element: <Home />
    },
    {
        path: "game",
        element: <Game />

    },
    {
        path: "login",
        element: <Login />
    },
    {
        path: "",
        element: <Navigate to="/home" />

    }
];

export const router = createHashRouter(routes);
