import WebpackBar from 'webpackbar';
import { CracoConfig } from '@craco/types';

const cracoConfig: CracoConfig = {
    webpack: {
        plugins: {
            add: [new WebpackBar()],
        },
        configure: {
            output: {
                publicPath: 'auto'
            },
        },
    },
};
export default cracoConfig;
